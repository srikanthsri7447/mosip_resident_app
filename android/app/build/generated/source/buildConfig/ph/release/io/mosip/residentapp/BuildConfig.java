/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.mosip.residentapp;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "io.mosip.residentapp";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "ph";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "untagged-1-17d5b32-dirty-ph";
}
